from celery import Celery
import pandas as pd
import time
from pprint import pprint
import requests
from bs4 import BeautifulSoup


class Result:
    input_url : str = None
    redirect_required : bool = False
    redirect_count : int = None
    
    redirect_urls : list = []
    redirect_urls_unique : list = []
    last_redirect_url : str = None

    status_code : int = None
    retrieved : bool = False

    img_urls_logo : list = []
    img_urls : list = []


    def to_row(self):
        row = {'input_url' : self.input_url,
               'redirect_required' : self.redirect_required,
               'redirect_count' : self.redirect_count,
               'redirect_urls' : self.redirect_urls,
               'redirect_urls_unique' : self.redirect_urls_unique,
               'last_redirect_url' : self.last_redirect_url,
               'status_code' : self.status_code,
               'retrieved' : self.retrieved ,
               'img_urls_logo' : self.img_urls_logo,
               'img_urls' : self.img_urls
                }
        return row



app = Celery('tasks',
              backend='redis://localhost',
              broker='amqp://guest:guest@localhost:5672//'
              )


@app.task
def scrape_img_urls(url):

    # Grab a list of urls from img tags
    try:
        r = requests.get(url, timeout=3)
        soup = BeautifulSoup(r.content, 'html5lib')
        tags = soup.findAll('img')
        img_urls = list(set(tag['src'] for tag in tags))
        return img_urls
    except:
        return []
    

@app.task
def get_result(input_url):

    # Create result object
    result = Result()   
    result.input_url = str(input_url)#f'http://{input_url}/' 
    
    # Attempt to pull the url
    try:
        r = requests.get(result.input_url, timeout=1)

        result.redirect_urls = [redirect.url for redirect in r.history]
        result.redirect_urls_unique = list(set(result.redirect_urls))
        result.redirect_count = len(result.redirect_urls)
        result.redirect_required = True if len(result.redirect_urls) > 0 else False
        result.last_redirect_url = result.redirect_urls[-1] if result.redirect_required else None
        result.status_code = r.status_code
        result.retrieved = True

 
        # See if the page can be scraped for image urls
        result.img_urls = scrape_img_urls(result.last_redirect_url) if result.redirect_required else scrape_img_urls(result.input_url)
        
        logos = []
        for url in result.img_urls:
            if 'logo' in url.lower():
                logos.append(url)

        result.img_urls_logo = logos 
        
    except:        
        result.retrieved = False

        

    return result.to_row()    


@app.task(bind=True)
def my_task(self):
    # update the state by one every second for 20 seconds
    for i in range(20):
        self.update_state(state='PROGRESS',
                          meta={'current': i, 'total': 20})
        time.sleep(1)

    return True
    